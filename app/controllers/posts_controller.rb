class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:show, :index]
  before_action :authenticate_admin!, only: [:new, :create, :destroy, :update, :edit]  

  # GET /posts
  # GET /posts.json
  def index
    @posts = Post.all
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
  #   require 'mathematical'
  #   require 'math-to-itex'

    # Convertir ecuaciones LaTeX en imágenes
  #  @contenidoeq = MathToItex(@post.contenido).convert do |eq, type|
  #    svg_content = Mathematical.new(:base64 => true).render(eq)
  #    con=svg_content[:data]
      # Crear tags para imagen con codificación base64
  #    %|<img class="#{type.to_s}-math" data-math-type="#{type.to_s}-math" src="#{con}"/>|
  #  end

  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
  end
  def mercury_update
  page = Page.find(params[:id])
  page.name = params[:content][:page_name][:value]
  page.content = params[:content][:page_content][:value]
  page.save!
  render text: ""
end
  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(post_params)

    respond_to do |format|
      if @post.save
        format.html { redirect_to @post, notice: 'Post was successfully created.' }
        format.json { render :show, status: :created, location: @post }
      else
        format.html { render :new }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to @post, notice: 'Post was successfully updated.' }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to posts_url, notice: 'Post was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    def authenticate_admin!
      if current_user.privilegios < 10
        redirect_to posts_path, notice: "No estás autorizado a acceder a esta sección" #, status: :unauthorized
      end     
    end
    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:titulo, :autor, :tags, :materia_id, :descripcion, :orden, :tiempo, :contenido, :bibliografia, :portada)
    end
end
