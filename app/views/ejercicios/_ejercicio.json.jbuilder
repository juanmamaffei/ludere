json.extract! ejercicio, :id, :tema_id_id, :consigna, :respuesta, :created_at, :updated_at
json.url ejercicio_url(ejercicio, format: :json)
