class Post < ApplicationRecord
  has_attached_file :portada, :styles => { :medium => "600x600>", :thumb => "200x200>" }, :default_url => "/images/:style/nocover.png"
  validates_attachment_content_type :portada, :content_type => /\Aimage\/.*\Z/
end
