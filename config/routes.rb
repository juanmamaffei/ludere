Rails.application.routes.draw do
  

  resources :ejercicios
  resources :temas
  devise_for :users
  resources :posts

  resources :materia
  root 'welcome#index'

  mathjax 'mathjax'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
