class CreateEjercicios < ActiveRecord::Migration[5.1]
  def change
    create_table :ejercicios do |t|
      t.references :tema, foreign_key: true
      t.text :consigna
      t.string :respuesta

      t.timestamps
    end
  end
end
