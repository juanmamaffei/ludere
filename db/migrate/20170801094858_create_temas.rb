class CreateTemas < ActiveRecord::Migration[5.1]
  def change
    create_table :temas do |t|
      t.references :materium, foreign_key: true
      t.references :post, foreign_key: true
      t.text :evaluacion
      t.string :titulo

      t.timestamps
    end
  end
end
