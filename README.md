# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version

* System dependencies

* Configuration

* Database creation

* Database initialization

* How to run the test suite

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...

Para el funcionamiento de Mathematical:
a) Instalación de dependencias
1) GNU Make

sudo apt-get update
sudo apt-get upgrade
sudo apt-get install build-essential
gcc -v
make -v
sudo apt-get install manpages-dev

2) glib-2.0
sudo apt-get install libperl-dev
sudo apt-get install libgtk2.0-dev
sudo apt-get install libglib2.0-dev

3) gdk-pixbuf-2.0
sudo apt-get install gir1.2-gdkpixbuf-2.0

4) xml2
sudo apt-get install xml2
sudo apt-get install libxml2-dev

5) cairo
sudo apt-get install libcairo2-dev

6) pango
sudo apt-get install libpango1.0-dev

7) cmake
sudo apt-get install cmake

8) bison
sudo apt-get install flex bison

9) python3
sudo apt-get install python3

10) mtex2mml
git clone https://github.com/gjtorikian/mtex2MML.git
cd mtex2MML
script/bootstrap
cd build
cmake ..
make

b) Clonación del repositorio e instalación
git clone https://github.com/gjtorikian/mathematical.git
cd mathematical
script/bootstrap
bundle exec rake compile

gem install mathematical

*** Crear usuario Postgre y base de datos
